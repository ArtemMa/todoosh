import {useState} from "react"
import { ITodo } from "../../interfaces/ITodo"
import './InputAddTodo.css'


interface IInputAddTodoProps {
    onCreate: (todo: ITodo) => void
}

export const InputAddTodo = ({onCreate}: IInputAddTodoProps) => {
    let [name, setName] = useState('')

    const createTodo = (event: React.FormEvent) => {
        event.preventDefault()
        onCreate({name: name, complete: false})
        setName('')
    }

    const changeName = (event: React.ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value)
    }

    return (
        <form onSubmit={createTodo}>
            <input type="text" required value={name} onChange={changeName} />
            <input type="submit" />
        </form>
    )
}