import {useState} from "react"
import { ITodo } from "../../interfaces/ITodo"
import './Todo.css'

interface ITodoProps {
    todo: ITodo
}

export const Todo = ({todo}: ITodoProps) => {
    let [complete, setComplete] = useState(false)

    let todoClasses = `todo ${complete ? 'todo--complete' : ''}`

    return (
        <div className={todoClasses}>
            <div className="todo-desc">{todo.name} - {complete ? 'completed' : 'new'}</div>
            <button onClick={() => {setComplete(prev => !prev)}}>{complete ? 'reset' : 'complete'}</button>
        </div>
    )
}