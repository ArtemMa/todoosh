import {useState} from 'react'
import { Todo } from '../../components/Todo/Todo'
import { ITodo } from '../../interfaces/ITodo'
import { InputAddTodo } from '../../components/InputAddTodo/InputAddTodo'
import './MainPage.css'

interface IMainPageProps {
    todo: ITodo
}

let todo = {
    name: 'firstTodo',
    complete: false
}

export function MainPage() {
    const [todos, setTodo] = useState<ITodo[]>([])

    const createTodo = (todo: ITodo) => {
        setTodo([...todos, todo])        
    }

    return (
        <div className='mainpage'>
            <h1>MainPage</h1>
            <InputAddTodo onCreate={createTodo} />
            {todos.map((todo, key) => <Todo todo={todo} key={key} />)}
        </div>
    )
}