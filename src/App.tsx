import React from 'react';
import { MainPage } from './pages/MainPage/MainPage';
import './App.css'
import {Route, Routes} from 'react-router-dom'
import { StartPage } from './pages/StartPage/StartPage';

function App() {
  return (
    <Routes>
      <Route path='/' element={<StartPage />} />
      <Route path='/main' element={<MainPage />} />
    </Routes>
    
  );
}

export default App;
